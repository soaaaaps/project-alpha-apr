from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView
from accounts.views import create_user


urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("signup/", create_user, name="signup"),
]
